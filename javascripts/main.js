$(function () {
	var $orders = $("#orders");
	var $name  	= $("#name");
	var $age  	= $("#age");

	var dataTemplate = $("#order-template").html();

	function addData(order) {
		$orders.append(Mustache.render(dataTemplate , order));
	}


	$.ajax({	
		type : 'GET',
		url : 'http://rest.learncode.academy/api/learncode/friends',
		headers: {"Access-Control-Allow-Origin" : "*"},
		success : function(orders) {
			console.log('success' , orders);
			$.each(orders , function(i, order) {
				addData(order);
			});
		},
		error : function() {
			alert('error get data');
		}
	});


	$("#add-data").on('click', function() {
		var data = {
			name 	: $name.val(),
			age 	: $age.val(),
		};

		$.ajax({
			type : 'POST',
			url : 'http://rest.learncode.academy/api/learncode/friends',
			data : data ,
			success : function (dataBaru) {
				addData(dataBaru);
				$name.val('');
				$age.val('');
			},
			error : function () {
				alert('Gagal tambah data');
			}
		});
	});

	$orders.delegate('.hapus','click', function() {

		var $li = $(this).closest('li');

		$.ajax({
			type : 'DELETE',
			url : 'http://rest.learncode.academy/api/learncode/friends/' + $(this).attr('data-id'),
			success : function () {
				$li.fadeOut('300', function() {
					$(this).remove();
				});
			}
		});
		
	});

	$orders.delegate('.editOrder', 'click', function() {
		var $li = $(this).closest('li');
		$li.find('input.name').val($li.find('span.name').html());
		$li.find('input.age').val($li.find('span.age').html());
		$li.addClass('edit');
	});

	$orders.delegate('.cancelEdit', 'click', function() {
		$(this).closest('li').removeClass('edit');
		
	});



	$orders.delegate('.saveEdit', 'click', function() {
		var $li = $(this).closest('li');

		var order = {
			name : $li.find('input.name').val(),
			age : $li.find('input.age').val()
		};

		$.ajax({
			type : "PUT" ,
			url : 'http://rest.learncode.academy/api/learncode/friends/' + $li.attr('data-id'),
			data : order,
			success : function (newOrder) {
				$li.find('span.name').html(order.name);
				$li.find('span.age').html(order.age);
				$li.removeClass('edit');
			},
			error : function(){
				alert('gagal update data');
			}
		});
	});


});